# Install/Upgrade
```bash
$ helm upgrade --install NOME_RELEASE -n NAMESPACE --create-namespace -f NOME_ARQUIVO_VALUES .
```

# Generate manifests (local)
```bash
$ helm template --debug NOME_RELEASE -n NAMESPACE -f NOME_ARQUIVO_VALUES .
```

# Structure
```bash
# ./ ------------------------------------------- > root;
#   templates/ --------------------------------- > generate Openshift/k8s manifests;
#   toml_files/ -------------------------------- > files used to create configMaps and secrets using Helm function "Files.Get";
#     configMap_values.toml -------------------- > contains values for the Deployment manifest to refer to the configMap;
#     secret_values.toml ----------------------- > contains values for the Deployment manifest to refer to the secret;
#     pipeline_secret_git_clone_values.toml ---- > contains values for the Pipeline, task GitClone, to inject user and password for the repository authentication;
#   values.yaml -------------------------------- > configurable values to be used by the templates to generate the manifests;
```

Reference: https://helm.sh/docs/topics/charts/