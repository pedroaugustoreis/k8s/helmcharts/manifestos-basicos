{{/* Generates metadata structure */}}
{{- define "metadata.generate" -}}
metadata:
  name: {{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    app: {{ .Release.Name }}
{{- end}}

{{- define "metadata.pipeline.generate" -}}
metadata:
  name: {{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    app: {{ .Release.Name }}-pipeline
{{- end}}